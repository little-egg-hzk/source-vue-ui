import request from "@/utils/request";

// 查询材料列表
export function listMaterials(query) {
  return request({
    url: "/system/materials/list",
    method: "get",
    params: query,
  });
}

// 查询材料详细
export function getMaterials(id) {
  return request({
    url: "/system/materials/" + id,
    method: "get",
  });
}

// 新增材料
export function addMaterials(data) {
  return request({
    url: "/system/materials",
    method: "post",
    data: data,
  });
}

// 修改材料
export function updateMaterials(data) {
  return request({
    url: "/system/materials",
    method: "put",
    data: data,
  });
}

// 删除材料
export function delMaterials(id) {
  return request({
    url: "/system/materials/" + id,
    method: "delete",
  });
}
